package com.devcamp.pizza365.repository;

import java.util.List;

import com.devcamp.pizza365.model.CCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);//tìm country theo country code, do là duy nhất nên chỉ trả về 1 Object 
	List<CCountry> findByCountryCodeContaining(String countryCode);//tìm country theo country code chứa chuỗi (like %)
	List<CCountry> findByCountryName(String countryName);//tim country theo country name, do có thể trả về nhiều nên dùng list hoặc ArrayList
	CCountry findByRegionsRegionCode(String regionCode);//tim country theo region code
	List<CCountry> findByRegionsRegionName(String regionName);
}
