package com.devcamp.pizza365.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepository;
    public CCountry createCountry(CCountry cCountry){
        try {
			CCountry savedRole = countryRepository.save(cCountry);
			//CCountry savedRole = countryRepository.save(cCountry);  đây là cách set trực tiếp không tạo 1 object mới
			return savedRole;
		} catch (Exception e) {
			return null;
		}
    }
}